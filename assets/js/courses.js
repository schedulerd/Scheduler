import { loadNavbar } from './navbar.js';
import { loadFooter } from './footer.js';

document.addEventListener('DOMContentLoaded', function() {
    loadNavbar(); 
    loadFooter();
    fetch('winter.csv')
        .then(response => response.text())
        .then(csvString => {
            const rows = csvString.split('\n');
            const header = rows.shift().split(',');

            let html = '<table>';
            html += '<tr>' + header.map(h => `<th>${h.trim()}</th>`).join('') + '</tr>';

            rows.forEach(row => {
                const cols = row.split(',');
                html += '<tr>' + cols.map(col => `<td>${col.trim()}</td>`).join('') + '</tr>';
            });

            html += '</table>';
            document.getElementById('csv-data-container').innerHTML = html;
        })
        .catch(error => console.error('Error fetching CSV:', error));
});





































































