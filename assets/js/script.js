// Import navbar and footer modules for page layout
import { loadNavbar } from './navbar.js';
import { loadFooter } from './footer.js';

// Function to generate a weekly schedule
function generateWeek() {
    // Get the container element where the schedule will be displayed
    const scheduleContainer = document.getElementById('scheduleContainer');
    // Define weekdays to be displayed in the schedule
    const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
    // Initialize a variable to hold HTML content
    let scheduleHTML = '';

    // Create the header row with days of the week
    scheduleHTML += '<div class="columns is-mobile"><div class="column has-text-centered has-text-weight-bold">Time/Day</div>';
    days.forEach(day => {
        scheduleHTML += `<div class="column has-text-centered has-text-weight-bold">${day}</div>`;
    });
    scheduleHTML += '</div>';

    // Generate schedule rows for each hour between 8 AM and 8 PM
    for (let hour = 8; hour <= 20; hour++) { 
        // Convert 24-hour format to 12-hour format
        let time = hour > 12 ? `${hour - 12} PM` : (hour === 12 ? `${hour} PM` : `${hour} AM`);
        // Start a new row for each hour
        scheduleHTML += '<div class="columns is-mobile"><div class="column has-text-centered">' + time + '</div>';
        // Create a cell for each day at the current hour
        for (let dayIndex = 0; dayIndex < days.length; dayIndex++) {
            const cellId = `cell-${hour}-${dayIndex}`;
            scheduleHTML += `<div class="column has-text-centered" id="${cellId}">-</div>`; 
        }
        scheduleHTML += '</div>';
    }

    // Inject the generated HTML into the schedule container
    scheduleContainer.innerHTML = scheduleHTML;
}

// Function to place courses from the shopping cart onto the timetable
function placeCoursesFromShoppingCart() {
    resetSchedule(); // Clear any existing courses on the schedule
    // Select all rows in the shopping cart table
    const shoppingCartRows = document.querySelectorAll('.content table tbody tr');

    // Iterate over each row to extract course details
    shoppingCartRows.forEach(row => {
        const courseDetails = {
            subjectCode: row.cells[0].innerText.split(" ")[0],
            courseNumber: row.cells[0].innerText.split(" ")[1],
            instructorName: row.cells[1].innerText,
            credits: row.cells[2].innerText,
            instructionMethod: row.cells[3].innerText,
            instructionType: row.cells[4].innerText,
            days: row.cells[5].innerText,
            startTime: row.cells[6].innerText.split(' - ')[0],
            endTime: row.cells[6].innerText.split(' - ')[1],
        };

        // Place each course on the timetable
        placeCourseOnTimetable(courseDetails);
    });
}

// Function to place a course on the timetable
function placeCourseOnTimetable(courseDetails) {
    const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
    const dayMapping = { 'Tuesday': 'T', 'Thursday': 'R' }; 
    // Convert start and end times to hours
    const startHour = timeStringToHour(courseDetails.startTime);
    const endHour = timeStringToHour(courseDetails.endTime);
    const duration = endHour - startHour;
    // Get a color for the course
    const color = getColorForCourse(courseDetails.subjectCode, courseDetails.courseNumber);

    // Split course days and iterate over them
    const days = courseDetails.days.split(', '); 

    days.forEach(dayName => {
        const dayIndex = daysOfWeek.indexOf(dayName);
        if (dayIndex !== -1) {
            // Place the course block on the timetable for each hour of its duration
            for (let hour = startHour; hour < startHour + duration; hour++) {
                const cellId = `cell-${hour}-${dayIndex}`;
                const cell = document.getElementById(cellId);
                if (cell) {
                    cell.style.backgroundColor = color;
                    cell.setAttribute('data-tippy-content', formatCourseTooltip(courseDetails));
                    cell.classList.add('tooltip-target');
                    // Set course text for the first hour block only
                    if (hour === startHour) {
                        cell.textContent = courseDetails.subjectCode + ' ' + courseDetails.courseNumber;
                        cell.classList.add('class-block-text');
                    }
                }
            }
        }
    });
}

// Object to store color mappings for courses
let courseColorMapping = {};

// Function to get a color for a course, assigning a new one if not already mapped
function getColorForCourse(subjectCode, courseNumber) {
    const courseKey = `${subjectCode} ${courseNumber}`;
    if (!courseColorMapping[courseKey]) {
        courseColorMapping[courseKey] = getRandomColor(); // Assign a new color
        console.log(`Assigned new color ${courseColorMapping[courseKey]} to course ${courseKey}`); 
    } else {
        console.log(`Using existing color ${courseColorMapping[courseKey]} for course ${courseKey}`); 
    }
    return courseColorMapping[courseKey];
}


// Function to generate a random RGBA color with high brightness
function getRandomColor() {
    const highValue = () => Math.floor(128 + Math.random() * 127); // Generate a random number for RGB
    return `rgba(${highValue()}, ${highValue()}, ${highValue()}, 0.5)`; // Return RGBA color string
}

// Function to initialize tooltips for schedule cells
function initializeTooltips() {
    // Use tippy.js to create tooltips for elements with 'class-block-text' class
    tippy(document.querySelectorAll('.class-block-text'), {
        allowHTML: true, // Allow HTML content in tooltips
        content(reference) {
            return reference.getAttribute('title'); // Use the title attribute as tooltip content
        },
        theme: 'custom-theme', // Set a custom theme for tooltips
    });
}
// Function to convert a time string (e.g., "2:00 PM") to a 24-hour format hour
function timeStringToHour(timeStr) {
    const [time, modifier] = timeStr.split(' ');
    let [hours, minutes] = time.split(':').map(Number);
    if (modifier.toUpperCase() === 'PM' && hours !== 12) hours += 12;
    return hours;
}


// Function to format a tooltip showing detailed course information
function formatCourseTooltip(courseDetails) {
    // Find the full details of the course from global data based on several matching criteria
    const fullCourseDetails = globalCoursesData.find(course => 
        course['Subject Code'] === courseDetails.subjectCode && 
        course['Course Number'] === courseDetails.courseNumber &&
        course['Instruction Type'] === courseDetails.instructionType &&
        course['Days'] === courseDetails.days);

    // If full course details are found, format and return them as HTML string for tooltip
    if (fullCourseDetails) {
        return `
            <strong>Course Title:</strong> ${fullCourseDetails['Course Title']}<br>
            <strong>Section:</strong> ${fullCourseDetails['Section']}<br>
            <strong>Instructor:</strong> ${fullCourseDetails['Instructor Name']}<br>
            <strong>Avg. RMP Rating:</strong> ${fullCourseDetails['Average RMP Rating']}<br>
            <strong>Instruction Type:</strong> ${fullCourseDetails['Instruction Type']}<br>
            <strong>Credits:</strong> ${fullCourseDetails['Credits']}
        `;
    } else {
        // Return a default message if course details are not found
        return 'Course details not found.';
    }
}

// Function to reset a select field to its default state
function resetSelectField(selectId, defaultText = "Select an option") {
    // Get the select element by its ID
    const select = document.getElementById(selectId);
    select.innerHTML = ''; // Clear existing options
    // Create and append the default option
    const defaultOption = document.createElement('option');
    defaultOption.textContent = defaultText;
    defaultOption.disabled = true;
    defaultOption.selected = true;
    select.appendChild(defaultOption);
}

// Function to reset dependent select fields to their default states
function resetDependentFields() {
    // Reset various select fields with their default prompt text
    resetSelectField('instructionMethodSelect', 'Select Instruction Method');
    resetSelectField('sectionSelect', 'Select Section');
    resetSelectField('labSelect', 'Select Lab');
    resetSelectField('recitationSelect', 'Select Recitation');
}

// Add an event listener to handle input in the course input field
document.getElementById('courseInput').addEventListener('input', function() {
    resetDependentFields(); // Reset dependent fields when the course input changes

    const selectedCourse = this.value; // Get the value of the selected course
    updateInstructorOptions(selectedCourse); // Update instructor options based on selected course
});

// Function to reset the entire schedule, clearing all course placements
function resetSchedule() {
    const hours = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]; // Define school hours
    const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']; // Define weekdays

    // Iterate over each day and hour to reset the cells in the schedule
    days.forEach((day, dayIndex) => {
        hours.forEach((hour) => {
            const cellId = `cell-${hour}-${dayIndex}`; // Construct cell ID
            const cell = document.getElementById(cellId); // Get the cell element
            if (cell) {
                cell.style.backgroundColor = ''; // Reset cell background color
                cell.textContent = '-'; // Reset cell text
                cell.classList.remove('class-block-text'); // Remove class block text styling
            }
        });
    });
}



// Global variable to store course data
let globalCoursesData = [];

// Function to fetch course data from a CSV file and process it
function fetchAndProcessCSV() {
    fetch('winter.csv') // Fetch the CSV file
        .then(response => response.text()) // Get the text response
        .then(csvText => {
            globalCoursesData = csvToArray(csvText); // Convert CSV text to array and store in global variable
            populateCourseOptions(globalCoursesData); // Populate course options based on fetched data
        });
}


// Function to convert CSV string to array of objects
function csvToArray(str, delimiter = ",") {
    // Extract headers by splitting the first line
    const headers = str.slice(0, str.indexOf("\n")).split(delimiter).map(header => header.trim());
    // Split the rest of the CSV into rows
    const rows = str.slice(str.indexOf("\n") + 1).split("\n");

    const results = []; // Array to hold the converted data

    // Process each row
    rows.forEach((row, rowIndex) => {
        // Regex to split row into values, respecting quotes
        let values = row.match(/(".*?"|[^",]+)(?=\s*,|\s*$)/g);

        // Check if row has correct number of values
        if (!values || values.length !== headers.length) {
            console.error(`Mismatched row in CSV at line ${rowIndex + 2}:`, row);
            return; // Skip rows with mismatched number of values
        }

        // Clean values by trimming and removing quotes
        values = values.map(value => value.trim().replace(/^"|"$/g, ''));

        // Create an object for each row
        const rowObject = headers.reduce((object, header, index) => {
            object[header] = values[index];
            return object;
        }, {});

        results.push(rowObject); // Add the object to results array
    });

    // Return the array, filtering out any undefined or null entries
    return results.filter(row => row); 
}

// Function to populate course options in the datalist
function populateCourseOptions(data) {
    const courseInput = document.getElementById('courseInput'); // Course input field
    const courseDatalist = document.getElementById('courseOptions'); // Datalist for course input

    // Extract unique courses from data and sort them
    let uniqueCourses = [...new Set(data.map(item => `${item['Subject Code']} ${item['Course Number']}`))].sort();
    uniqueCourses.forEach(course => {
        let optionElement = document.createElement('option');
        optionElement.value = course;
        courseDatalist.appendChild(optionElement); // Append each course as an option
    });

    // Add event listener to filter options based on input
    courseInput.addEventListener('input', function() {
        filterCourseOptions(this.value, uniqueCourses);
        updateCourseTitle(this.value); // Update course title based on selected course
    });
}

// Function to filter course options in the datalist based on input
function filterCourseOptions(inputValue, courses) {
    const courseDatalist = document.getElementById('courseOptions'); // Get the course datalist
    courseDatalist.innerHTML = ''; // Clear existing options

    // Filter courses based on the input value and populate datalist
    courses.filter(course => course.toLowerCase().startsWith(inputValue.toLowerCase()))
            .forEach(filteredCourse => {
                let optionElement = document.createElement('option');
                optionElement.value = filteredCourse;
                courseDatalist.appendChild(optionElement); // Append matching courses as options
            });
}

// Function to update the course title input field based on the selected course
function updateCourseTitle(selectedValue) {
    const courseTitleInput = document.getElementById('courseTitleInput'); // Course title input field
    const selectedSubjectCode = selectedValue.split(" ")[0];
    const selectedCourseNumber = selectedValue.split(" ")[1];

    // Find the matching course in global data
    const matchingCourse = globalCoursesData.find(course => 
        course['Subject Code'] === selectedSubjectCode && course['Course Number'] === selectedCourseNumber
    );

    // Update the course title input field
    if (matchingCourse) {
        courseTitleInput.value = matchingCourse['Course Title'];
        adjustInputWidth(courseTitleInput); // Adjust input width based on text length
    } else {
        courseTitleInput.value = ''; // Clear the field if no match is found
    }
}

// Function to adjust the width of an input field based on its content
function adjustInputWidth(input) {
    const text = input.value || input.placeholder; // Get text from input or placeholder
    const canvas = document.createElement('canvas'); // Create a canvas element
    const context = canvas.getContext('2d'); // Get canvas context
    context.font = getComputedStyle(input).font; // Set font to match input field's font
    const width = context.measureText(text).width; // Measure text width
    input.style.width = `${width + 30}px`; // Set input width based on text width
}

// Function to reset a select element
function resetSelect(selectId) {
    let select = document.getElementById(selectId); // Get the select element by ID
    select.innerHTML = ''; // Clear all options from the select element
}

// Function to update instructor options based on selected course
function updateInstructorOptions(selectedCourse) {
    const instructorSelect = document.getElementById('instructorSelect'); // Instructor select field
    resetSelect('instructorSelect'); // Reset the instructor select field

    // Extract subject code and course number from the selected course
    const [subjectCode, courseNumber] = selectedCourse.split(" ");
    // Filter global data to find matching courses
    const filteredData = globalCoursesData.filter(item => 
        item['Subject Code'] === subjectCode && item['Course Number'] === courseNumber
    );

    // Extract unique instructors from filtered data
    let uniqueInstructors = [...new Set(filteredData.map(item => item['Instructor Name']))].sort();

    // Placeholder option for the instructor select field
    let placeholderOption = document.createElement('option');
    placeholderOption.textContent = "Select Instructor";
    placeholderOption.disabled = true;
    placeholderOption.selected = true;
    instructorSelect.appendChild(placeholderOption);

    // Populate instructor select with options
    uniqueInstructors.forEach(instructor => {
        if (instructor !== 'Unknown Instructor') {
            let optionElement = document.createElement('option');
            optionElement.value = instructor;
            optionElement.textContent = instructor;
            instructorSelect.appendChild(optionElement);
        }
    });

    // Add event listener to update lab and recitation options based on instructor selection
    instructorSelect.addEventListener('change', function() {
        const selectedInstructor = this.value;
        const availableLabsAndRecitations = findAvailableLabsAndRecitations(selectedCourse, selectedInstructor);
        populateLabAndRecitationOptions(availableLabsAndRecitations);
    });
}

// Function to update the instruction method options based on the selected instructor
function updateInstructionMethodOptions(selectedInstructor) {
    const instructionMethodSelect = document.getElementById('instructionMethodSelect');
    resetSelect('instructionMethodSelect'); // Clear previous options

    if (selectedInstructor && selectedInstructor !== 'Unknown Instructor') {
        // Filter data for the selected instructor
        const filteredData = globalCoursesData.filter(item => item['Instructor Name'] === selectedInstructor);
        // Extract and sort unique instruction methods
        let uniqueMethods = [...new Set(filteredData.map(item => item['Instruction Method']))].sort();
        uniqueMethods.forEach(method => {
            // Create and append options for each instruction method
            let optionElement = document.createElement('option');
            optionElement.value = method;
            optionElement.textContent = method;
            instructionMethodSelect.appendChild(optionElement);
        });
    }
}

// Function to update section options based on selected course and instructor
function updateSectionOptions(selectedCourse, selectedInstructor) {
    const sectionSelect = document.getElementById('sectionSelect');
    resetSelect('sectionSelect'); // Clear previous options

    // Extract subject code and course number from the selected course
    const [subjectCode, courseNumber] = selectedCourse.split(" ");
    // Filter data for sections matching the selected course and instructor, and for lectures
    const filteredSections = globalCoursesData.filter(item =>
        item['Subject Code'] === subjectCode &&
        item['Course Number'] === courseNumber &&
        item['Instructor Name'] === selectedInstructor &&
        item['Instruction Type'] === 'Lecture'); 

    // Create and append options for each filtered section
    filteredSections.forEach(section => {
        let optionElement = document.createElement('option');
        optionElement.value = section['Section'];
        optionElement.textContent = section['Section'];
        sectionSelect.appendChild(optionElement);
    });
}

// Function to find available labs and recitations based on selected course and instructor
function findAvailableLabsAndRecitations(selectedCourse, selectedInstructor) {
    // Extract subject code and course number from the selected course
    const [subjectCode, courseNumber] = selectedCourse.split(" ");
    // Filter data for labs and recitations matching the selected course and instructor without time conflicts
    return globalCoursesData.filter(item =>
        item['Subject Code'] === subjectCode &&
        item['Course Number'] === courseNumber &&
        item['Instructor Name'] === selectedInstructor &&
        (item['Instruction Type'] === 'Lab' || item['Instruction Type'] === 'Recitation') &&
        !timeConflictWithMainCourse(item));
}

// Function to populate lab and recitation options from available items
function populateLabAndRecitationOptions(labsAndRecitations) {
    const labSelect = document.getElementById('labSelect');
    const recitationSelect = document.getElementById('recitationSelect');
    resetSelect('labSelect');
    resetSelect('recitationSelect');

    let hasLab = false, hasRecitation = false;

    // Append each lab or recitation to the corresponding select field
    labsAndRecitations.forEach(item => {
        let optionElement = document.createElement('option');
        optionElement.value = item['Section'];
        optionElement.textContent = item['Section']; 

        if (item['Instruction Type'] === 'Lab') {
            labSelect.appendChild(optionElement);
            hasLab = true;
        } else if (item['Instruction Type'] === 'Recitation') {
            recitationSelect.appendChild(optionElement);
            hasRecitation = true;
        }
    });

    // Add placeholder options if no labs or recitations are available
    if (!hasLab) {
        labSelect.appendChild(new Option("No lab", "No lab", true, true));
    }
    if (!hasRecitation) {
        recitationSelect.appendChild(new Option("No recitation", "No recitation", true, true));
    }
}

// Function to check for time conflicts with the main course
function timeConflictWithMainCourse(item) {
    // Function to convert time string to a number for comparison
    const convertTimeToNumber = (timeStr) => {
        const [time, modifier] = timeStr.split(' ');
        let [hours, minutes] = time.split(':').map(Number);
        if (modifier === 'PM' && hours !== 12) hours += 12;
        return hours + minutes / 60; 
    };

    // Find the main lecture course based on selected course input
    const selectedCourseValue = document.getElementById('courseInput').value;
    const [selectedSubjectCode, selectedCourseNumber] = selectedCourseValue.split(" ");
    const mainCourse = globalCoursesData.find(course =>
        course['Subject Code'] === selectedSubjectCode &&
        course['Course Number'] === selectedCourseNumber &&
        course['Instruction Type'] === 'Lecture' 
    );

    if (!mainCourse) return false; // No conflict if the main course is not found

    // Convert start and end times to numbers for comparison
    const mainStart = convertTimeToNumber(mainCourse['Start Time']);
    const mainEnd = convertTimeToNumber(mainCourse['End Time']);
    const itemStart = convertTimeToNumber(item['Start Time']);
    const itemEnd = convertTimeToNumber(item['End Time']);

    // Check for time conflict
    return !(itemEnd <= mainStart || itemStart >= mainEnd);
}

// Add event listener to the course input field to update options based on input
document.getElementById('courseInput').addEventListener('input', function() {
    const selectedCourse = this.value; // Get the value of the selected course
    updateInstructorOptions(selectedCourse); // Call function to update instructor options based on selected course
});

// Add event listener to the instructor select field to update options based on selection
document.getElementById('instructorSelect').addEventListener('change', function() {
    const selectedCourse = document.getElementById('courseInput').value; // Get the value of the selected course
    const selectedInstructor = this.value; // Get the value of the selected instructor

    // Call functions to update instruction method and section options
    updateInstructionMethodOptions(selectedInstructor);
    updateSectionOptions(selectedCourse, selectedInstructor);

    // Find available labs and recitations for the selected course and instructor
    const availableLabsAndRecitations = findAvailableLabsAndRecitations(selectedCourse);
    // Populate lab and recitation options based on available options
    populateLabAndRecitationOptions(availableLabsAndRecitations);
});

// Function to initialize the shopping cart table if it doesn't exist
function initializeShoppingCartTable() {
    const shoppingCartContainer = document.querySelector('.content');
    // Check if a table already exists in the shopping cart container
    if (!shoppingCartContainer.querySelector('table')) {
        const table = document.createElement('table');
        table.classList.add('table'); // Add 'table' class for styling
        // Set the inner HTML of the table with headers
        table.innerHTML = `
            <thead>
                <tr>
                    <th>Subject/Course</th>
                    <th>Instructor</th>
                    <th>Credits</th>
                    <th>Instr. Method</th>
                    <th>Instr. Type</th>
                    <th>Days</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        `;
        shoppingCartContainer.appendChild(table); // Append the table to the shopping cart container
    }
}

// Function to add selected course to the shopping cart
function addToShoppingCart() {
    const selectedCourseValue = document.getElementById('courseInput').value;
    const selectedInstructor = document.getElementById('instructorSelect').value;
    const selectedSection = document.getElementById('sectionSelect').value;

    const shoppingCartContainer = document.querySelector('.content');
    const [subjectCode, courseNumber] = selectedCourseValue.split(" ");
    // Find the main lecture course based on the selected values
    const selectedMainCourse = globalCoursesData.find(course => 
        course['Subject Code'] === subjectCode && 
        course['Course Number'] === courseNumber &&
        course['Instructor Name'] === selectedInstructor &&
        course['Section'] === selectedSection &&
        course['Instruction Type'] === 'Lecture'
    );

    // If the main course is found, add it and its associated labs and recitations to the shopping cart
    if (selectedMainCourse) {
        addCourseToShoppingCart(selectedMainCourse, shoppingCartContainer);

        // Find available labs and recitations
        const labsAndRecitations = findAvailableLabsAndRecitations(selectedCourseValue, selectedInstructor);

        // Check for labs and recitations and add them if no time conflict with the main course
        const lab = labsAndRecitations.find(course => course['Instruction Type'] === 'Lab');
        const recitation = labsAndRecitations.find(course => course['Instruction Type'] === 'Recitation');

        if (lab && !timeConflictWithMainCourse(lab, selectedMainCourse)) {
            addCourseToShoppingCart(lab, shoppingCartContainer);
        }

        if (recitation && !timeConflictWithMainCourse(recitation, selectedMainCourse)) {
            addCourseToShoppingCart(recitation, shoppingCartContainer);
        }
    }
}

// Function to add a course row to the shopping cart table
function addCourseToShoppingCart(course, container) {
    const tbody = container.querySelector('tbody'); // Get the table body of the shopping cart table
    const row = document.createElement('tr'); // Create a new table row
    const timePeriod = formatTimePeriod(course['Start Time'], course['End Time']); // Format the time period

    // Set the inner HTML of the row with course details
    row.innerHTML = `
        <td>${course['Subject Code']} ${course['Course Number']}</td>
        <td>${course['Instructor Name']}</td>
        <td>${course['Credits']}</td>
        <td>${course['Instruction Method']}</td>
        <td>${course['Instruction Type']}</td>
        <td>${course['Days']}</td>
        <td>${timePeriod}</td>
    `;
    tbody.appendChild(row); // Append the row to the table body
}

// Function to format the time period as a string
function formatTimePeriod(startTime, endTime) {
    return `${formatTime(startTime)} - ${formatTime(endTime)}`;
}

// Function to format a time string from 24-hour to 12-hour format
function formatTime(time) {
    const [hours, minutes] = time.split(':');
    const hoursInt = parseInt(hours);
    const period = hoursInt >= 12 ? 'pm' : 'am';
    const formattedHours = hoursInt > 12 ? hoursInt - 12 : hoursInt;
    return `${formattedHours}:${minutes} ${period}`;
}

// Add event listener to the 'Generate Schedule' button to place courses from the shopping cart onto the timetable
document.getElementById('generateScheduleButton').addEventListener('click', function() {
    placeCoursesFromShoppingCart(); // Call function to place courses from shopping cart
    initializeTooltips(); // Initialize tooltips for the schedule
});

// Initialize the application when the DOM is fully loaded
document.addEventListener('DOMContentLoaded', (event) => {
    loadNavbar(); // Load the navigation bar
    loadFooter(); // Load the footer
    generateWeek(); // Generate the weekly schedule layout
    fetchAndProcessCSV(); // Fetch course data from CSV and process it
    initializeShoppingCartTable(); // Initialize the shopping cart table

    // Toggle navbar menu for mobile view
    const navbarBurger = document.getElementsByClassName("navbar-burger")[0];
    const navbarMenu = document.getElementById("mainNavigationBar");
    navbarBurger.addEventListener("click", () => {
        navbarBurger.classList.toggle("is-active");
        navbarMenu.classList.toggle("is-active");
    });

    // Add event listener to the 'Add to Cart' button to add courses to the shopping cart
    document.querySelector('.button.is-info').addEventListener('click', addToShoppingCart);
});





































































