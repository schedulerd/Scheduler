// Function to load the footer into the HTML body
function loadFooter() {
    // HTML string for the footer, including inline style
    const footerHTML = `
    <style>
        /* Styling for the footer */
        .footer {
            position: relative; /* Position relative for absolute positioning of before element */
            clear: both; /* Clear floats to ensure the footer is below all content */
            padding: 5px 0px 0px 10px; /* Padding for spacing within the footer */
            font-size: 0.8rem; /* Smaller font size for footer text */
            background: white; /* Background color of the footer */
        }
        /* Pseudo-element for styling a top border line */
        .footer::before {
            content: ''; /* Required for before and after pseudo-elements */
            position: absolute; /* Absolute position relative to the footer */
            top: 0; /* Position at the top of the footer */
            left: 10px; /* Left alignment with some spacing */
            right: 10px; /* Right alignment with some spacing */
            border-top: 1px solid grey; /* Top border styling */
            height: 1px; /* Height of the border line */
        }
    </style>
    <footer class="footer">
        <!-- Footer content with copyright notice -->
        Copyright ©2023 Ansh Bhajjan | Lauren Gumiit | Daniil Neniukov | Vanessa Trinh
    </footer>
    `;
    // Insert the footer HTML at the end of the document body
    document.body.insertAdjacentHTML('beforeend', footerHTML);
}

// Export the loadFooter function to make it available for import in other modules
export { loadFooter };






































































