// Importing the footer module for use in this script
import { loadFooter } from './footer.js';

// Function to toggle between the login and registration form
function toggleForm(isSignUp) {
    // Selecting elements from the DOM for manipulation
    const container = document.querySelector('.container .box');
    const title = document.querySelector('.title');
    const accountStatus = document.querySelector('.account-action');

    // Check if the form should be in 'sign up' mode
    if (isSignUp) {
        // Changing the title to indicate registration
        title.textContent = 'DREXEL SCHEDULER REGISTRATION';
        // Setting inner HTML to show registration fields
        container.innerHTML = `
            <div class="field">
                <label class="label" for="drexel-email">DREXEL EMAIL</label>
                <div class="control">
                    <input class="input" type="email" id="drexel-email" name="drexel-email" required>
                </div>
            </div>
            <div class="field">
                <label class="label" for="password">PASSWORD</label>
                <div class="control">
                    <input class="input" type="password" id="password" name="password" required>
                </div>
            </div>
            <div class="field">
                <label class="label" for="repeat-password">REPEAT PASSWORD</label>
                <div class="control">
                    <input class="input" type="password" id="repeat-password" name="repeat-password" required>
                </div>
            </div>
            <button class="button is-link is-fullwidth" id="register-button">REGISTER</button>
        `;
        // Changing the account action text for users who already have an account
        accountStatus.innerHTML = 'Already have an account? <a class="has-text-info" id="signin-button">Sign in</a>';
    } else {
        // If not sign up mode, revert to login form
        title.textContent = 'DREXEL SCHEDULER LOGIN';
        container.innerHTML = `
            <div class="field">
                <label class="label" for="drexel-email">DREXEL EMAIL</label>
                <div class="control">
                    <input class="input" type="email" id="drexel-email" name="drexel-email" required>
                </div>
            </div>
            <div class="field">
                <label class="label" for="password">PASSWORD</label>
                <div class="control">
                    <input class="input" type="password" id="password" name="password" required>
                </div>
            </div>
            <button class="button is-link is-fullwidth" id="login-button">LOGIN</button>
            <p class="has-text-centered">
                <a href="/forgot-password">Forgot your password?</a>
            </p>
        `;
        // Changing the account action text for users who need to sign up
        accountStatus.innerHTML = 'Need an account? <a class="has-text-info" id="signup-button">Sign up</a>';
    }

    // Attach event listeners to elements in the form
    attachEventListeners();
}

// Function to attach event listeners to form elements
function attachEventListeners() {
    // Selecting buttons and links by their IDs
    const signinButton = document.getElementById('signin-button');
    const signupButton = document.getElementById('signup-button');
    const loginButton = document.getElementById('login-button');
    const registerButton = document.getElementById('register-button');

    // Adding event listeners for sign in and sign up buttons
    if (signinButton) signinButton.addEventListener('click', () => toggleForm(false));
    if (signupButton) signupButton.addEventListener('click', () => toggleForm(true));

    // Adding event listeners for login and register buttons
    if (loginButton) loginButton.addEventListener('click', validateDrexelEmail);
    if (registerButton) registerButton.addEventListener('click', validateRegistration);
}

// Function to validate the Drexel email format
function validateDrexelEmail() {
    // Getting the value of the email input field
    var email = document.getElementById('drexel-email').value;
    // Checking if the email ends with the Drexel domain
    if(!email.endsWith("@drexel.edu")) {
        // Alert if the email is not a valid Drexel email
        alert("Please enter a valid Drexel email address.");
    }
}

// Function to validate registration data
function validateRegistration() {
    // Getting values from the input fields
    var email = document.getElementById('drexel-email').value;
    var password = document.getElementById('password').value;
    var repeatPassword = document.getElementById('repeat-password').value;
    var errorMessage = '';

    // Checking if the email is a valid Drexel email
    if (!email.endsWith("@drexel.edu")) {
        errorMessage += "Please enter a valid Drexel email address.\n";
    }

    // Checking if the passwords match
    if (password !== repeatPassword) {
        errorMessage += "Passwords do not match.\n";
    }

    // Password validation conditions
    var hasDigit = /\d/.test(password);
    var hasSymbol = /[!@#$%^&*(),.?":{}|<>]/.test(password);
    var hasUppercase = /[A-Z]/.test(password);
    var hasLowercase = /[a-z]/.test(password);
    var isLongEnough = password.length >= 12;

    // Checking if the password meets all the conditions
    if (!hasDigit || !hasSymbol || !hasUppercase || !hasLowercase || !isLongEnough) {
        errorMessage += "Password must contain at least one digit, one symbol, one uppercase letter, one lowercase letter, and be at least 12 characters long.\n";
    }

    // Displaying an error message if there are validation errors
    if (errorMessage.length > 0) {
        alert(errorMessage.trim());
    }
}

// Event listener to initialize the page after the DOM content is loaded
document.addEventListener('DOMContentLoaded', (event) => {
    loadFooter();  // Loading the footer
    toggleForm(false);  // Initializing the page with the login form
});





































































