// Function to load the navigation bar into the HTML body
function loadNavbar() {
    // Get the current path of the window (URL)
    const currentPath = window.location.pathname;

    // Function to check if a given page name is part of the current path
    // Returns true if the current path includes the page name, indicating an active page
    const isActive = (pageName) => {
        return currentPath.includes(pageName);
    };
   
    // HTML string for the navigation bar
    // Uses template literals for embedding JavaScript expressions (e.g., isActive function)
    var navbarHTML = `
    <nav class="navbar mt-1" role="navigation">
        <div class="navbar-brand">
            <!-- Navbar brand/logo section -->
            <a class="navbar-item" href="./index.html">
                <!-- Link to the home page with an image as the logo -->
                <img src="https://upload.wikimedia.org/wikipedia/commons/6/61/Drexel-logo.png" />
            </a>
            <!-- Navbar burger icon for mobile responsiveness -->
            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="mainNavigationBar">
                <!-- Three spans for the burger icon, used in mobile view -->
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>
        <!-- Main navigation menu -->
        <div class="navbar-menu" id="mainNavigationBar">
            <div class="navbar-start">
                <!-- Navigation items, with active class applied dynamically based on the current URL -->
                <a class="navbar-item ${isActive('view-all-courses') ? 'is-active' : ''}" href="/view-all-courses.html" style="margin-right: 0.625rem;">View All Courses</a>
                <a class="navbar-item ${isActive('generate-schedule.html') ? 'is-active' : ''}" href="/generate-schedule.html" style="margin-right: 0.625rem;">Generate Schedule</a>
                <a class="navbar-item ${isActive('saved-schedule') ? 'is-active' : ''}" href="/saved-schedule.html">Saved Schedule</a>
            </div>
        </div>
    </nav>`;
    // Insert the navbar HTML at the beginning of the document body
    document.body.insertAdjacentHTML('afterbegin', navbarHTML);
  }
  
  // Export the loadNavbar function to be available for import in other modules
  export { loadNavbar };





































































